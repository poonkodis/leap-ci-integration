package com.leadics.leap.common;

import java.io.Serializable;
import java.util.List;

public interface LPHomeIF<T extends Serializable> {
	
	public Class<T> getEntityClass();
	public void persist(T transientInstance);
	public T  merge(T  detachedInstance);
	//public T saveOrUpdate(T detachedInstance);
	public void attachDirty(T  instance);
	public void delete(T  persistentInstance);
	//public T  findById(ID id) ;
	public List<T> findAll();

}
