package com.leadics.leap.common;

public class LPConstants {
	public static final Integer SESSIONTIMEOUT = 12000;
	public static final String YES = "Y";
	public static final String NO = "N";
	public static final Character USERENABLED = 'A';
	public static final Character USERDISABLED = 'D';
}
