package com.leadics.leap.common;

import java.util.List;

import com.leadics.leap.dto.UserCtx;

public abstract class LPResource {

	private UserCtx userCtx;
	
	protected void setUserCtx(UserCtx user) {
		this.userCtx = user;
	}
	
	protected UserCtx getUserCtx() {
		return this.userCtx;
	}
	
	public abstract LPDto fetchInfo(String name);
	public abstract LPDto saveInfo(LPDto lpdto);
	public abstract LPDto updateInfo(LPDto lpdto);
	public abstract List<LPDto> fetchList();
}
