package com.leadics.leap.common;

import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;




public abstract class LPService {
	
	private static final Log log = LogFactory.getLog(LPService.class);
	
	public abstract LPDto fetchInfo(String name); 
	  
	public abstract LPDto saveInfo(LPDto lpDto);
	  
	public abstract LPDto updateInfo(LPDto lpDto);
	 
	public abstract List<LPDto> fetchList();
	 
}
