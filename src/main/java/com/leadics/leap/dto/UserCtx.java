package com.leadics.leap.dto;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.leadics.leap.dto.RoleDto;

public class UserCtx implements Principal {

	 private String id, firstName, lastName, login, email, password;

	 private List<String> role;
	 private Set<RoleDto> roleSet;
	 
	 public void setId(String id) {this.id = id;}
	 public String getId() {return this.id;}
	 public String getFirstName() {return this.firstName;}
	 public void setFirstName(String firstName) {this.firstName = firstName;}
	 public String getLastName() {return this.lastName;}
	 public void setLastName(String lastName) {this.lastName = lastName;}
	 public String getLogin() {return login;}
	 public void setLogin(String login) {this.login = login;}
	 public String getEmail() {return email;}
	 public void setEmail(String email) {this.email = email;}
	 public String getPassword() {return password;}
	 public void setPassword(String password) {this.password = password;}
	 

	 public List<String> getRole() {return role;}
	 public void setRole(List<String> roleSet) {this.role = roleSet;}
	 public Set<RoleDto> getRoleSet() {return roleSet;}
		 public void setRoleSet(Set<RoleDto> roleSet) {this.roleSet = roleSet;}
	 private static Log logger = LogFactory.getLog(UserCtx.class.getName());    
	

	public UserCtx(String login, List<String> role) { 
		this.login = login;
		this.role = role;
	}
	

	public UserCtx() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public String getName() {
		return this.firstName+" "+this.lastName;
	}
}

