package com.leadics.leap.dto;

import java.security.Principal;
import javax.ws.rs.core.SecurityContext;
import com.leadics.leap.dto.UserCtx;

public class SecurityCtx implements SecurityContext{

	private UserCtx user;
    private String scheme;
    
    public SecurityCtx(UserCtx user,String scheme) {
    	this.user = user;
    	this.scheme = scheme;
    }
    
	@Override
	public String getAuthenticationScheme() {
		return SecurityContext.BASIC_AUTH;
	}

	@Override
	public Principal getUserPrincipal() {
		return this.user;
	}

	@Override
	public boolean isSecure() {
		return "https".equals(this.scheme); 
	}

	@Override
	public boolean isUserInRole(String role) {
		if (user.getRole() != null) {
            return user.getRole().contains(role);
        }
        return false;
	}

}
