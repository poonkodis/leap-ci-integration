package com.leadics.leap.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.leadics.leap.common.LPDto;
import com.leadics.leap.dao.record.UserRole;

public class UserDto extends LPDto{

	private Integer userid;
	private String username;
	private Character status;
	private Character notify;
	private Date createdat;
	private String createdby;
	private Date modifiedat;
	private String modifiedby;
	private Set<UserRole> userRoles = new HashSet<UserRole>(0);
	
	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Character getStatus() {
		return status;
	}
	public void setStatus(Character status) {
		this.status = status;
	}
	public Character getNotify() {
		return notify;
	}
	public void setNotify(Character notify) {
		this.notify = notify;
	}
	public Date getCreatedat() {
		return createdat;
	}
	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getModifiedat() {
		return modifiedat;
	}
	public void setModifiedat(Date modifiedat) {
		this.modifiedat = modifiedat;
	}
	public String getModifiedby() {
		return modifiedby;
	}
	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}
	public Set<UserRole> getUserRoles() {
		return userRoles;
	}
	public void setUserRoles(Set<UserRole> userRoles) {
		this.userRoles = userRoles;
	}
	
	
}
