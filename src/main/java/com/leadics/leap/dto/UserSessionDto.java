package com.leadics.leap.dto;

import com.leadics.leap.common.LPDto;

public class UserSessionDto extends LPDto {
	
	private Integer sessid;
	private int userid;
	private String starttime;
	private String endtime;
	
	public Integer getSessid() {
		return sessid;
	}
	public void setSessid(Integer sessid) {
		this.sessid = sessid;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}



}
