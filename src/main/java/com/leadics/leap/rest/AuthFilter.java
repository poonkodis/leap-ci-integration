package com.leadics.leap.rest;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Priority;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Priorities;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.leadics.leap.common.LPDto;
import com.leadics.leap.dao.record.Role;
import com.leadics.leap.dao.record.User;
import com.leadics.leap.dao.record.UserRole;
import com.leadics.leap.dao.record.UserSession;
import com.leadics.leap.dto.RoleDto;
import com.leadics.leap.dto.SecurityCtx;
import com.leadics.leap.dto.UserSessionDto;
import com.leadics.leap.dto.UserRoleDto;
import com.leadics.leap.dto.UserDto;
import com.leadics.leap.dto.UserCtx;
import com.leadics.leap.services.AuthService;
import com.leadics.leap.services.UserService;

//import javassist.bytecode.Descriptor.Iterator;

//provider is for auto-discovery and prematching is to indicate that this should be evaluated before being directed to the jersey resource requested
//@Priority(Priorities.AUTHENTICATION)
@Provider 

//@Priority(1)
@PreMatching


public class AuthFilter implements ContainerRequestFilter {

	public static final String AUTHENTICATION_HEADER = "Authorization";
	private static final Response ACCESS_DENIED = Response.status(Response.Status.UNAUTHORIZED).entity("You need to login").build();
	private static final Response ACCESS_FORBIDDEN = Response.status(Response.Status.FORBIDDEN).entity("You cannot access this resource").build();
	private static final String AUTHENTICATION_SCHEME = "Basic";
	
	private static Log logger = LogFactory.getLog(AuthFilter.class.getName());   
	
	 public AuthFilter() {
		 logger.info("Initializing Custom Authorization Filter...");
	 }


    @Override
    public void filter(ContainerRequestContext requestContext) throws WebApplicationException {
        //GET, POST, PUT, DELETE, ...
   	final MultivaluedMap<String, String> headers = requestContext.getHeaders();
    	final List<String> authorization = headers.get(AUTHENTICATION_HEADER);
    	String xToken = headers.getFirst("XTOKEN");
        String path = requestContext.getUriInfo().getPath(true);
        String sessionID = "";
        if (path != null && !(path.equals("users/login"))) {
        	
        	Cookie cookie = null;
        	for (Cookie c : requestContext.getCookies().values()) {
        		if (c.getName().equals("XSRF-TOKEN")) { 	    	
        			sessionID = c.getValue();
        			break;
        		}
        	}

        	//If no sessionID, block access
        	if(sessionID == null || sessionID == "") {
           		requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity("You need to login").build());
        		return;
        	}

        	try {

        		UserCtx userCtx = setSecurityContext(sessionID);
        		String scheme = requestContext.getUriInfo().getRequestUri().getScheme();
        		requestContext.setSecurityContext(new SecurityCtx(userCtx, scheme));

        	} catch (Exception e) {
        		throw new WebApplicationException(Status.UNAUTHORIZED);
        	}

        }
        
   }
    


   private UserCtx setSecurityContext(String sessionID) throws Exception {
    	AuthService authentication = new AuthService();
    	UserSession ush  = authentication.authenticate(sessionID);
    	if (ush == null || ush.getUserid() == 0) {
    		throw new WebApplicationException(Status.UNAUTHORIZED);
    	}

	    String userId = new Integer(ush.getUserid()).toString();
	    UserService userService = new UserService();
	    UserDto userDto = (UserDto) userService.fetchInfo(userId);
	    final String username = userDto.getUsername();
	    UserCtx userCtx = null;
//	    UserCtx userCtx = new UserCtx(username,rolenames);
//	    userCtx.setId(userId);
//	    userCtx.setRoleSet(roleSet);
	    return userCtx;
	   
	   
    }



}

