package com.leadics.leap.rest;

import java.io.IOException;
import java.util.Iterator;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.Provider;

import com.leadics.leap.rest.ResponseConstructor;
import com.leadics.leap.common.LPConstants;
@Provider 

public class ResponseFilter implements ContainerResponseFilter {

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		// TODO Auto-generated method stub
		int timeout =  LPConstants.SESSIONTIMEOUT.intValue();
		String path = requestContext.getUriInfo().getPath(true);
	    if (path == null || (path.equals("users/login"))) return;
	    
	    if (path.equals("users/logout")) timeout = 0;
		Iterator iter = requestContext.getCookies().values().iterator();
		while (iter.hasNext()) {
			Cookie c = (Cookie)iter.next();
			if (c.getName().equals("XSRF-TOKEN")) {
				NewCookie newcookie = new NewCookie(c.getName(), c.getValue(), "/", "", "updated session", timeout, false);
				responseContext.getHeaders().add("Set-Cookie",newcookie);
				break;
			}
		}
		

	}

}
