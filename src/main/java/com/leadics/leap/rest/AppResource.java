package com.leadics.leap.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.leadics.leap.common.LPDto;
import com.leadics.leap.common.LPResource;

public class AppResource extends LPResource{

    @GET
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetch")
 	@Override
	public LPDto fetchInfo(String name) {
		// TODO Auto-generated method stub
		return null;
	}

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("save")
	@Override
	public LPDto saveInfo(LPDto lpdto) {
		// TODO Auto-generated method stub
		return null;
	}

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("update/{id}/")
	@Override
	public LPDto updateInfo(LPDto lpdto) {
		// TODO Auto-generated method stub
		return null;
	}

    @POST
    @Produces(MediaType.APPLICATION_JSON)
	@Override
	public List<LPDto> fetchList() {
		// TODO Auto-generated method stub
		return null;
	}

}
