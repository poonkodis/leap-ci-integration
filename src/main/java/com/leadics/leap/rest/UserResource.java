package com.leadics.leap.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.leadics.leap.common.LPDto;
import com.leadics.leap.common.LPResource;
import com.leadics.leap.dto.UserDto;

/**
 * 
 */
@Path("user")
public class UserResource extends LPResource {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     *  
     */
    @GET
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("fetch")
    @Override
    public UserDto fetchInfo(String username) {
        return null;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("save")
    @Override
    public UserDto saveInfo(LPDto user) {
    	
		return null;
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("update/{id}/")
    @Override
    public UserDto updateInfo(LPDto user) {
		return null;
    	
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("verify")
    public UserDto verifyLogin(UserDto user) {
    	return null;
    }


    @POST
    @Produces(MediaType.APPLICATION_JSON)
	@Override
	public List<LPDto> fetchList() {
		// TODO Auto-generated method stub
		return null;
	}
    
    
}
