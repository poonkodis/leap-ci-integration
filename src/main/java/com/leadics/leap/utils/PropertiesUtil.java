package com.leadics.leap.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PropertiesUtil {
	public static Map<String, String> properties = new HashMap<String, String>();
	
	static {
		Properties prop = new Properties();
		Log log = LogFactory.getLog(PropertiesUtil.class);
		try {
			String resourceName = "application.properties";
			String resourceNameSockets = "socket-msgs.properties";
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			try(InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
				prop.load(resourceStream);
			}
			
			Enumeration<?> enumeration = prop.propertyNames();
			while(enumeration.hasMoreElements()) {
				String key = (String) enumeration.nextElement();
				String value = prop.getProperty(key);
				properties.put(key, value);
				log.debug("key = "+key+" value = "+value);
			}
			try(InputStream resourceStream = loader.getResourceAsStream(resourceNameSockets)) {
				prop.load(resourceStream);
			}
			Enumeration<?> enumerationSockets = prop.propertyNames();
			while(enumerationSockets.hasMoreElements()) {
				String key = (String) enumerationSockets.nextElement();
				String value = prop.getProperty(key);
				properties.put(key, value);
				log.debug("key = "+key+" value = "+value);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
