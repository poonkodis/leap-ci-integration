package com.leadics.leap.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

import org.hibernate.type.descriptor.java.CalendarDateTypeDescriptor;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateUtil {
	
	public static final String YYYYMMDD = "yyyyMMdd";
	
	public static String getCurrentDateTime() {
		String pattern = "MM/dd/yyyy HH:mm:ss";
		String dt = DateTime.now().toString(DateTimeFormat.forPattern(pattern));
		return dt;
	}
	
	public static Date getCurrentDate() throws ParseException {
		String pattern = "MM/dd/yyyy HH:mm:ss";
		String dt = DateTime.now().toString(DateTimeFormat.forPattern(pattern));
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		Date date = format.parse(dt);
		return date;
	}
	
	public static String getCurrentTime(String outputFormat) throws IllegalArgumentException, Exception {
		        //Create current date object
		        Date currentDateTime = new Date();

		        //Apply formatting
		        return formatDate(currentDateTime, outputFormat);
	}
	
	
		    
	public static Date convertStringToDate(String date) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
		DateTime dt = formatter.parseDateTime(date);
		return dt.toDate();
	}
	//yyyy-MM-dd HH:mm:ss Z
	public static Date convertStringToDateTimeZone(String date) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss Z");
		DateTime dt = formatter.parseDateTime(date);
		return dt.toDate();
	}
	
	public static String convertDateToString(java.util.Date inputDate, String format) {
		if (inputDate == null) return null;
		if (format == null || format.equals("")) {
			format = "dd-MM-yyyy HH:mm:ss";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(inputDate);
	}
	
	
	
	public static String formatDate(java.util.Date inputDate,String outputFormat) throws IllegalArgumentException, Exception {
	        //Validate input date value
	        if (inputDate == null) {
	            throw new IllegalArgumentException("Input date cannot "
	                    + " be null in DateUtils.formatDate method");
	        }

	        //Validate output date format
	        if (outputFormat == null) {
	            throw new IllegalArgumentException("Output format cannot"
	                    + " be null in DateUtils.formatDate method");
	        }

	        //Apply formatting
	        SimpleDateFormat formatter = new SimpleDateFormat(outputFormat);
	        return formatter.format(inputDate);
	 }
	
	public static Date addMinutesToDate(Date date, int minutes) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MINUTE, minutes);
		Date addedDate = cal.getTime();
		return addedDate;
	}
	
	public static boolean dateDifferenceNegOrPos(Date date1, Date date2) {
		if(date1 == null || date2 == null) {
			return false;
		}
		Instant start = date1.toInstant();
		Instant end = date2.toInstant();
		Duration duration = Duration.between(start, end);
		duration.toMinutes();
		return duration.isNegative();
	}
	
	public static Duration dateDifference(Date date1, Date date2) {
		if(date1 == null || date2 == null) {
			return null;
		}
		Instant start = date1.toInstant();
		Instant end = date2.toInstant();
		Duration duration = Duration.between(start, end);
		return duration;
		
	}
	
	public static Date getTimeAtStartOfTheDay() {
		DateTime today = new DateTime().withTimeAtStartOfDay();
		return today.toDate();
	}
	
	public static Date getTimeAtStartOfDayTomorrow() {
		DateTime today = new DateTime().withTimeAtStartOfDay();
		DateTime tomorrow = today.plusDays(1).withTimeAtStartOfDay().minusSeconds(1);
		return tomorrow.toDate();
	}
}
