package com.leadics.leap.utils;

import org.apache.commons.codec.binary.Base64;

public class SecurityUtil {
	public String encodeBase64(String password) {
		if(password == null || password.isEmpty())
			return null;
		String encodedString = null;
		
		encodedString = new String(Base64.encodeBase64(password.getBytes()));
		return encodedString;
	}
	
	public String decodeBase64(String password) {
		if(password == null || password.isEmpty())
			return null;
		String encodedString = password;
		String decodedString = new String(Base64.decodeBase64(encodedString.getBytes()));
		return decodedString;
	}
}
