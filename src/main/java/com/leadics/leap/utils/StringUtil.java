package com.leadics.leap.utils;

import java.util.List;

public class StringUtil {

	public static boolean isStrNull(String str) {
		if (str == null || str.equals("")) {
			return true;
		} else {
			return false;
		}
	}
	
	
	
	public  String returnStrEmpty(String str) {
		if (str == null || str.equals("")) {
			return "";
		} else {
			return str;
		}
	}
	
	public int returnStrAsInt(String str)	{
		if (str == null || str.equals("")) {
			return 0;
		} else {
			return Integer.parseInt(str);
		}
	}
	
	 public static String noNull(Object value)	{
	    	if (value == null) return "";
	    	return value.toString();
	 }
	 


	 
	public static String returnListAsQStr(List<String> list,String separator) {
		String listStr = "";
		for (String s : list) {
		    listStr += "'"+s + "'" + separator;
		}
		if (listStr != null || !listStr.equals("")) {
			listStr = listStr.substring(0, listStr.length() - 1);
		}
		return listStr;

	}
}
