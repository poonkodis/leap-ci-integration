package com.leadics.leap.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.leadics.leap.common.LPHomeIF;

public abstract class LPHome<T extends Serializable>  implements LPHomeIF<T>{
	
	private static final Log log = LogFactory.getLog(LPHomeIF.class);

	//@PersistenceContext
	protected EntityManager entityMgr;
	private Class<T> dtoClass;
	
		

	public LPHome() {
		//this.dtoClass = dtoClass;
		 this.dtoClass = (Class<T>) ((ParameterizedType) getClass()  
                .getGenericSuperclass()).getActualTypeArguments()[0];  
	}
	

	@SuppressWarnings("unchecked")
	public void setEntityManager(EntityManager entityMgr) {
		 this.entityMgr = entityMgr;
	}
	
	@SuppressWarnings("unchecked")
	public EntityManager getEntityManager() {
		 return this.entityMgr;
	}
	
	@SuppressWarnings("unchecked")
	public void closeEntityManager() {
		 this.entityMgr.close();
	}
	
	public Class<T> getEntityClass() {
		return dtoClass;
	}
	
	@SuppressWarnings("unchecked")
	public void persist(T transientInstance) {
		 entityMgr.persist(transientInstance);
	}
	
	@SuppressWarnings("unchecked")
	public T  merge(T  detachedInstance) {
		 return entityMgr.merge(detachedInstance);
	}

	
	@SuppressWarnings("unchecked")
	protected T mapEntity(T entity,T transientInstance) {
		entity = transientInstance;
		return entity;
	}
	
	public void attachDirty(T  instance) {
		entityMgr.merge(instance);
	}
	

	
	public void delete(T  persistentInstance) {
		entityMgr.remove(persistentInstance);
	}
	
	
	protected List<T> findByCriteria(CriteriaQuery<T> cquery) {  
		cquery.select(cquery.from(dtoClass));
		return entityMgr.createQuery(cquery).getResultList();
	 }  
	 
	public List<T> findAll() {
		CriteriaQuery<T> cquery = entityMgr.getCriteriaBuilder().createQuery(dtoClass);
		cquery.select(cquery.from(dtoClass));
		return entityMgr.createQuery(cquery).getResultList();
	
	}
}
