package com.leadics.leap.dao;
/*
 * JobAccidentsHome.java
 *
 * Copyright (c) LeadICS 
 *
 *This class represents factory for obtaining the object of the class passed as argument and initialise the entity manager
 *
 * Project Name             : AutoScope
 * Module                   : Dao
 * Author                   : Indu  
 * Date                     : 12 june, 2017
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */

import javax.persistence.EntityManager;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.leadics.leap.common.LPHomeIF;

public class LPHomeFactory {

    public static final SessionFactory sessionFactory  =  getSessionFactory(); 

	public static LPHomeFactory getInstance() {
        try {
        	//System.out.println("we are getting LPHomeFactory getInstance");
            return new LPHomeFactory();
        } catch (Exception ex) {
            throw new RuntimeException("LPHomeFactory.getInstance failed: " );
        }
	}
    
	public LPHomeIF getHome(Class homeClass) {  
        try {  
        	LPHome home = (LPHome)homeClass.newInstance();
        	home.setEntityManager(getEM());
        	return home;  
        } catch (Exception ex) {  
            throw new RuntimeException("LPHomeFactory.getHome failed: " + homeClass, ex);  
        }  
    }  
	
    private static SessionFactory  getSessionFactory() {
        //return sessionFactory.createEntityManager();

    	 try {
             Configuration configuration = new Configuration();
             configuration.configure();
             // Create the SessionFactory from hibernate.cfg.xml
             ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                     configuration.getProperties()).build();
             return configuration.buildSessionFactory();
         }
         catch (Throwable ex) {
             // Make sure you log the exception, as it might be swallowed
             System.out.println("LPHomeFactory.getSessionFactory failed." + ex);
             throw new ExceptionInInitializerError(ex);
         }
    }
    protected static EntityManager getEM() {
        return sessionFactory.createEntityManager();
    }

    protected static void closeEM(EntityManager em) {
        em.close();
    }




}
