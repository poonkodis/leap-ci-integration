package com.leadics.leap.dao;
// Generated Jan 20, 2017 11:27:12 AM by Hibernate Tools 5.2.0.CR1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.persistence.Subgraph;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.leadics.leap.dao.record.Role;
import com.leadics.leap.dao.record.User;



/**
 * Home object for domain model class Users.
 * 
 * @see com.leadics.UserDto.dto.Users
 * @author Hibernate Tools
 */
public class UserHome  extends LPHome<User> {

	private static final Log log = LogFactory.getLog(UserHome.class);
	
	protected UserHome() {
		super();
	}

	public void persist(User transientInstance) {

		int userId = transientInstance.getUserid();
		ObjectMapper mapper = new ObjectMapper();
		try {
			entityMgr.getTransaction().begin();
			User user = entityMgr.find(User.class, userId);
			
			if (user != null) {
				user = transientInstance;
				entityMgr.merge(user);
				log.debug("UserHome.persist merge successful");
			} else {
				entityMgr.persist(transientInstance);
				log.debug("UserHome.persist successful");
			}
			
			entityMgr.getTransaction().commit();

		} catch (RuntimeException re) {
			log.error("UserHome.persist failed", re);
			entityMgr.getTransaction().rollback();
			throw re;
		}
	}


	public void attachClean(User instance) {
		try {
			entityMgr.lock(instance, LockModeType.NONE);
			log.debug("UserHome.attachClean successful");
		} catch (RuntimeException re) {
			log.error("UserHome.attachclean failed", re);
			throw re;
		}
	}


	public User findById(Integer id) {
		log.debug("getting Users instance with id: " + id);
		if(id == null) {
			return null;
		}
		try {
			if(!entityMgr.getTransaction().isActive())
				entityMgr.getTransaction().begin();
				User instance = (User) entityMgr.find(User.class, id);
//			EntityGraph<User> graph = this.entityMgr.createEntityGraph(User.class);
//			Subgraph userRoles = graph.addSubgraph("userRoleses");
//			Subgraph roles = userRoles.addSubgraph("roles");
//			Map hints = new HashMap();
//			hints.put("javax.persistence.loadgraph", graph);
//			User instance = (User) entityMgr.find(User.class, id,hints);
			log.debug("UserHome.findById successful, instance"+instance);
			return instance;
		} catch (RuntimeException re) {
			log.error("UserHome.findById failed", re);
			throw re;
		}
	}
 
	public List<User> findAllEm(){
		try{
			List<User> allUsers = entityMgr.createQuery("from Users u",User.class)
					.getResultList();
			return allUsers;
		}catch(Exception e){
			log.debug("Exception in VehicleModelsHome -> allmodels :"+e.getMessage());
			return null;
		}
		
		
	}
	
	
	public User findByIdEm(int id) {
		log.debug("getting Users instance with id: " + id);
		try {
			entityMgr.getTransaction().begin();
			User instance = entityMgr.createQuery("FROM Users u WHERE u.userId = :userId", User.class)
					.setParameter("userId", id).getResultList().get(0);
			
			if (instance == null) {
				log.debug("UserHome.findByIdEm successful, no instance found");
			} else {
				log.debug("UserHome.findByIdEm successful, instance found");
			}
			return instance;
		} catch (NoResultException nre) {
			log.error("UserHome.findByIdEm No result exists for user id : " + id);
			return null;
		} catch (RuntimeException re) {
			log.error("UserHome.findByIdEm failed", re);
			throw re;
		}
	}

	public List findByExample(User instance) {
		try {
			CriteriaBuilder builder = entityMgr.getCriteriaBuilder();
			CriteriaQuery<User> criteria = builder.createQuery(User.class);
			List results = entityMgr.createQuery(criteria).getResultList();
			log.debug("UserHome.findByExample successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("UserHome.findByExample failed", re);
			throw re;
		}
	}

	public User findByUserEmailAndPassword(String email, String encodedPassword) {
		try {
			if (!entityMgr.getTransaction().isActive()) {
				entityMgr.getTransaction().begin();
			}
			EntityGraph graph = entityMgr.createEntityGraph(User.class);
			Subgraph userroles = graph.addSubgraph("userRoleses");
			Subgraph roles =userroles.addSubgraph("roles");
			Map hints = new HashMap();
			hints.put("javax.persistence.loadgraph", graph);
			User user = entityMgr
					.createQuery("SELECT u FROM Users u WHERE u.email=:email AND u.password=:password", User.class)
					.setParameter("password", encodedPassword).setParameter("email", email).setHint("javax.persistence.fetchgraph", graph).getSingleResult();

			if (user == null) {
				log.debug("UserHome.findByUserEmailAndPassword successful, no instance found");
			} else {
				log.debug("UserHome.findByUserEmailAndPassword successful, instance found");
			}
			return user;
		} catch (NoResultException nre) {
			return null;
		} catch (RuntimeException re) {
			log.error("UserHome.findByUserEmailAndPassword failed", re);
			throw re;
		}
	}

	@Override
	public List<User> findAll() {
		try {
			entityMgr.getTransaction().begin();
			EntityGraph graph = entityMgr.createEntityGraph(User.class);
			Subgraph userSkillses = graph.addSubgraph("userSkillses");
			Subgraph userRoles = graph.addSubgraph("userRoleses");
			Subgraph roles = userRoles.addSubgraph("roles");
			Subgraph leaves = graph.addSubgraph("userLeaveses");
			Subgraph uskills = graph.addSubgraph("userSkillses");
			Subgraph skills = uskills.addSubgraph("skills");
			Subgraph techBType = graph.addSubgraph("technicianBaytypeses");
			Subgraph bayTypes = techBType.addSubgraph("bayTypes");
			
			Map hints = new HashMap();
			hints.put("javax.persistence.loadgraph", graph);
			List<User> results = this.entityMgr.createQuery(" FROM Users ", User.class).setHint("javax.persistence.fetchgraph", graph).getResultList();
			
			log.debug("UserHome.findAll found" + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("UserHome.findAll", re);
			throw re;
		}
	}


	
	public List<User> findByRole(Role role) {
		log.debug("getting Users with a role " + role.getRolename());
		try {
			entityMgr.getTransaction().begin();
			EntityGraph graph = entityMgr.createEntityGraph(User.class);
			Subgraph userRoles = graph.addSubgraph("userRoleses");
			Subgraph roles = userRoles.addSubgraph("roles");
			
			Map hints = new HashMap();
			hints.put("javax.persistence.loadgraph", graph);
	
			 
			List<User> userList = entityMgr
					.createQuery("SELECT DISTINCT u FROM Users u INNER JOIN u.userRoleses ur WHERE ur.id.roleId=:roley", User.class)
					.setParameter("roley", role.getRoleid()).setHint("javax.persistence.fetchgraph", graph).getResultList();

			if (userList == null) {
				log.debug("UserHome.findByRole successful, no instance found");
			} else {
				log.debug("UserHome.findByRole successful, instance found");
			}
			return userList;
		} catch (NoResultException nre) {
			return null;
		} catch (RuntimeException re) {
			log.error("UserHome.findByRole failed", re);
			throw re;
		}
	}

	public User findByUserEmail(String email) {
		log.debug("getting Users instance with email " + email);
		try {
			if (!entityMgr.getTransaction().isActive()) {
				entityMgr.getTransaction().begin();
			}
			
			EntityGraph graph = entityMgr.createEntityGraph(User.class);
			Subgraph userroles = graph.addSubgraph("userRoleses");
			Subgraph roles =userroles.addSubgraph("roles");
			Map hints = new HashMap();
			hints.put("javax.persistence.loadgraph", graph);
			
			User user = entityMgr.createQuery("from Users v where v.email = :email",User.class)
					.setParameter("email", email)
					.setHint("javax.persistence.fetchgraph", graph)
					.getSingleResult();

			if (user == null) {
				log.debug("UserHome.findByUserEmailAndPassword successful, no instance found");
			} else {
				log.debug("UserHome.findByUserEmailAndPassword successful, instance found");
			}
			return user;
		} catch (NoResultException nre) {
			return null;
		} catch (RuntimeException re) {
			log.error("UserHome.findByUserEmailAndPassword failed", re);
			throw re;
		}
	}
}
