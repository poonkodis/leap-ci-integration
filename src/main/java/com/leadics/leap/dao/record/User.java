package com.leadics.leap.dao.record;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.leadics.leap.common.LPRecord;


public class User  extends LPRecord{
	
	private Integer userid;
	private String username;
	private Character status;
	private Character notify;
	private Date createdat;
	private String createdby;
	private Date modifiedat;
	private String modifiedby;
	private Set<UserRole> userRoles = new HashSet<UserRole>(0);

	public User() {
	}

	public User(String username, Date createdat) {
		this.username = username;
		this.createdat = createdat;
	}

	public User(String username, Character status, Character notify, Date createdat, String createdby, Date modifiedat,
			String modifiedby, Set<UserRole> userRoles) {
		this.username = username;
		this.status = status;
		this.notify = notify;
		this.createdat = createdat;
		this.createdby = createdby;
		this.modifiedat = modifiedat;
		this.modifiedby = modifiedby;
		this.userRoles = userRoles;
	}

	public Integer getUserid() {
		return this.userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Character getStatus() {
		return this.status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	public Character getNotify() {
		return this.notify;
	}

	public void setNotify(Character notify) {
		this.notify = notify;
	}

	public Date getCreatedat() {
		return this.createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public String getCreatedby() {
		return this.createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Date getModifiedat() {
		return this.modifiedat;
	}

	public void setModifiedat(Date modifiedat) {
		this.modifiedat = modifiedat;
	}

	public String getModifiedby() {
		return this.modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public Set<UserRole> getUserRoles() {
		return this.userRoles;
	}

	public void setUserRoles(Set<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

}
