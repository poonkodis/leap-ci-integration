package com.leadics.leap.dao.record;

import java.util.Date;

import com.leadics.leap.common.LPRecord;

public class Application extends LPRecord {
	private Integer applid;
	private String applname;
	private String applurl;
	private Character status;
	private Date modifiedat;
	private String modifiedby;

	public Application() {
	}

	public Application(String applname, String applurl, Character status, Date modifiedat, String modifiedby) {
		this.applname = applname;
		this.applurl = applurl;
		this.status = status;
		this.modifiedat = modifiedat;
		this.modifiedby = modifiedby;
	}

	public Integer getApplid() {
		return this.applid;
	}

	public void setApplid(Integer applid) {
		this.applid = applid;
	}

	public String getApplname() {
		return this.applname;
	}

	public void setApplname(String applname) {
		this.applname = applname;
	}

	public String getApplurl() {
		return this.applurl;
	}

	public void setApplurl(String applurl) {
		this.applurl = applurl;
	}

	public Character getStatus() {
		return this.status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	public Date getModifiedat() {
		return this.modifiedat;
	}

	public void setModifiedat(Date modifiedat) {
		this.modifiedat = modifiedat;
	}

	public String getModifiedby() {
		return this.modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

}
