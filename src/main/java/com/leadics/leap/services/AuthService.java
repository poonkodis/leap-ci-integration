package com.leadics.leap.services;

import java.text.ParseException;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.leadics.leap.dao.LPHomeFactory;
import com.leadics.leap.dao.UserSessionHome;
import com.leadics.leap.dao.UserHome;
import com.leadics.leap.dao.record.UserSession;
import com.leadics.leap.dto.UserDto;
import com.leadics.leap.dto.UserCtx;
import com.leadics.leap.utils.SecurityUtil;
import com.leadics.leap.utils.DateUtil;

public class AuthService {
	UserSessionHome userSessionHome;
	private static final Log log = LogFactory.getLog(AuthService.class);
	private LPHomeFactory ghf;
	public AuthService() {
		this.ghf = LPHomeFactory.getInstance();
		this.userSessionHome = null;
	}

	
	public String issueToken(UserCtx userCtx,String sessionkey) throws ParseException {
		//String sessionID = null;
		try {
		this.userSessionHome = (UserSessionHome) ghf.getHome(UserSessionHome.class);
			UserSession userSession = new UserSession();
			SecurityUtil secUtil = new SecurityUtil();
			sessionkey = secUtil.encodeBase64(sessionkey);
			userSession.setStarttime(DateUtil.getCurrentDateTime());
			userSession.setSesskey(sessionkey);
			userSession.setUserid(Integer.parseInt(userCtx.getId()));
			userSessionHome.getEntityManager().getTransaction().begin();
			userSessionHome.persist(userSession);
			userSessionHome.getEntityManager().getTransaction().commit();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			this.userSessionHome.closeEntityManager();
			this.userSessionHome = null;
		}
		return sessionkey;
	}
	

	public UserSession authenticate(String sessionId) throws Exception {
		this.userSessionHome = (UserSessionHome) this.ghf.getHome(UserSessionHome.class);
		UserSession ush = null;
		try {
			ush = userSessionHome.findBySessionId(sessionId);
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			this.userSessionHome.closeEntityManager();
		}
		return ush;
	}

	public boolean logoutUser(String sessionId) {
		try {
			this.userSessionHome = (UserSessionHome) ghf.getHome(UserSessionHome.class);
			UserSession ush = this.userSessionHome.findBySessionId(sessionId);
			this.userSessionHome.getEntityManager().getTransaction().begin();
			this.userSessionHome.delete(ush);
			this.userSessionHome.getEntityManager().getTransaction().commit();

			return true;
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			return false;
		} finally {
			this.userSessionHome.closeEntityManager();
			this.userSessionHome = null;
		}
	}
}
